import time

from config import POSTGRES
from main import process_queue, logger
from minicli import cli, run
from postgis.psycopg import register
from validators import check_coordinates


@cli
def initdb():
    """Initialize DB table."""
    with POSTGRES.cursor() as cur:
        cur.execute(
            'CREATE TABLE IF NOT EXISTS data ('
            '    id serial PRIMARY KEY,'
            '    coordinates geography(PointZ) NOT NULL,'
            '    time timestamp with time zone NOT NULL,'
            '    registration varchar(128) NOT NULL,'
            '    allowed boolean NULL DEFAULT true,'
            '    allowed_at timestamp with time zone NULL'
            ');')
        cur.execute(
            'CREATE TABLE IF NOT EXISTS reports ('
            '    key varchar(32) PRIMARY KEY,'
            '    data bytea NOT NULL,'
            '    errors jsonb NOT NULL,'
            '    time timestamp with time zone NOT NULL'
            ');')
    POSTGRES.commit()


@cli
def check_all():
    """(Re)check all unchecked positions against skunk."""
    register(POSTGRES)
    with POSTGRES.cursor() as cur:
        cur.execute('SELECT * FROM data WHERE allowed_at IS NULL')
        updates = 0
        for record in cur.fetchall():
            payload = check_coordinates({'coordinates': record[1]}, 1)
            allowed = payload['allowed']
            allowed_at = payload['allowed_at']
            if allowed_at is not None:
                cur.execute((
                    'UPDATE data '
                    'SET allowed=%s, allowed_at=%s '
                    'WHERE id=%s'), (allowed, allowed_at, record[0]))
                updates += 1
        print('{updates} records checked.'.format(updates=updates))
    POSTGRES.commit()


@cli
def process():
    logger.info('Running')
    while 1:
        process_queue()
        time.sleep(1)


if __name__ == '__main__':
    run()
