from datetime import datetime, timezone
import ujson as json
import pytest

from psycopg2.extras import DictCursor
import responses

from config import CHECK_URL, POSTGRES, REDIS
from cli import initdb

# Default allowed positions
ALLOWED_POSITION = [43.57, 5.71, 0.0]


def pytest_configure(config):
    initdb()


def pytest_runtest_setup():
    REDIS.zremrangebyrank('queue', 0, -1)
    with POSTGRES.cursor() as cursor:
        cursor.execute('TRUNCATE TABLE data;')
        cursor.execute('TRUNCATE TABLE reports;')
    POSTGRES.commit()


@pytest.fixture
def redis():
    return REDIS


@pytest.fixture
def factory():

    def _(**kwargs):
        data = {
            'time': datetime.now(timezone.utc).isoformat(),
            'registration': '1234',
            'coordinates': ALLOWED_POSITION
        }
        data.update(kwargs)
        score = datetime.now(timezone.utc).timestamp()
        data = json.dumps(data)
        REDIS.zadd('queue', score, data)
        return data.encode()

    return _


@pytest.fixture
def fetchone():

    def _(table_name='data'):
        with POSTGRES.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(
                'SELECT * FROM {} ORDER BY time DESC;'.format(table_name))
            return dict(cursor.fetchone())

    return _


@pytest.fixture
def count():

    def _(table_name='data'):
        with POSTGRES.cursor() as cursor:
            cursor.execute('SELECT count(*) FROM {};'.format(table_name))
            return cursor.fetchone()[0]

    return _


@pytest.fixture(scope='function')
def check_allowed():
    with responses.RequestsMock() as rmock:
        url = CHECK_URL.format(*ALLOWED_POSITION)
        rmock.add(responses.GET, url, json={'allowed': True},
                  match_querystring=True)
        yield rmock
