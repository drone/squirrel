import hashlib
import json
import logging
from datetime import datetime, timezone

import psycopg2
from config import POSTGRES, REDIS
from postgis import Point
from postgis.psycopg import register
from validators import validate

logger = logging.getLogger('squirrel')
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
logger.addHandler(handler)

WSG84 = 4326

register(POSTGRES)


def process_queue():
    data = REDIS.zrange('queue', 0, -1, withscores=True)
    parse_data(data)
    REDIS.zremrangebyrank('queue', 0, len(data) - 1)


def parse_data(data):
    for payload, received_at in data:
        original_payload = payload
        payload, errors = validate(payload, received_at)
        if errors:
            save_reports(errors, original_payload, received_at)
        else:
            save_data(payload)


def save_data(data):
    point = Point(*data['coordinates'], srid=WSG84)
    time_ = data['time']
    registration = data['registration']
    allowed = data['allowed']
    allowed_at = data['allowed_at']
    with POSTGRES.cursor() as cur:
        try:
            cur.execute(
                'INSERT INTO "data" '
                '    (coordinates, time, registration, allowed, allowed_at) '
                'VALUES (%s, %s, %s, %s, %s)',
                (point, time_, registration, allowed, allowed_at))
        except (psycopg2.DataError, psycopg2.InternalError) as e:
            logger.error('%s => %s', e, data)
            POSTGRES.rollback()
    POSTGRES.commit()


def save_reports(errors, payload, received_at):
    key = hashlib.md5(payload).hexdigest()
    time_ = datetime.utcfromtimestamp(received_at).replace(tzinfo=timezone.utc)
    with POSTGRES.cursor() as cur:
        errors = json.dumps(errors)
        try:
            cur.execute(
                'INSERT INTO "reports" (key, data, errors, time) '
                'VALUES (%s, %s, %s, %s)'
                'ON CONFLICT (key) DO UPDATE '
                'SET data=excluded.data, errors=excluded.errors, '
                'time=excluded.time',
                (key, payload, errors, time_))
        except (psycopg2.DataError, psycopg2.InternalError, ValueError) as e:
            logger.error('%s => %s', e, payload.decode(errors='ignore'))
            POSTGRES.rollback()
    POSTGRES.commit()
