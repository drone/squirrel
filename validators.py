import json
from datetime import datetime, timedelta, timezone

from dateutil import parser
import requests

from config import CHECK_URL

mandatory_keys = {'time', 'registration', 'coordinates'}
validators = []


class TypedException(Exception):
    """Set the type attribute if any, default to `format`."""

    def __init__(self, *args, **kwargs):
        self.type = kwargs.pop('type', 'format')
        super().__init__(*args, **kwargs)


class FormatException(TypedException):
    """Raised if the payload format is invalid."""


class ContentException(TypedException):
    """Raised if the payload content is invalid."""


def validator(func):
    # Warning, function order is important.
    validators.append(func)
    return func


def validate(payload, received_at):
    errors = {}
    for validator in validators:
        try:
            payload = validator(payload, received_at)
        except FormatException as e:
            errors.update({e.type: str(e)})
            return payload, errors
        except ContentException as e:
            errors.update({e.type: str(e)})
    return payload, errors


@validator
def validate_payload(payload, received_at):
    try:
        return json.loads(payload.decode().strip('\x00'))
    except ValueError:
        raise FormatException('Malformed payload {}'.format(payload))


@validator
def validate_keys(payload, received_at):
    data_keys = payload.keys()
    if not mandatory_keys <= data_keys:
        raise FormatException('Missing mandatory keys {} in {}'.format(
                              mandatory_keys, payload))
    if len(data_keys) > len(mandatory_keys):
        raise FormatException('Too many keys {} in {}'.format(
                              data_keys, payload))
    return payload


@validator
def validate_time(payload, received_at):
    value = payload['time']
    try:
        if isinstance(value, (int, float)):
            value = datetime.utcfromtimestamp(value)
        else:
            value = parser.parse(value)
    except ValueError:
        raise ContentException('Invalid time "{}"'.format(value), type='time')
    if not value.tzinfo:
        value = value.replace(tzinfo=timezone.utc)
    now = datetime.now(timezone.utc)
    if not value <= now + timedelta(minutes=5):
        raise ContentException(
            ('Time is in the future (might be a timezone issue), '
             'time must always be in UTC. '
             'Received time {}, current one {}.').format(value, now),
            type='time')
    received_at = (datetime.utcfromtimestamp(received_at)
                           .replace(tzinfo=timezone.utc))
    latency = received_at - value
    if latency > timedelta(minutes=30):
        # Nicer formatting.
        latency = latency - timedelta(microseconds=latency.microseconds)
        raise ContentException(
            ('Time is in the past (might be a time synchronization issue). '
             'Latency is too high (> 30 minutes): {}. '
             'Received time {}, current one {}.').format(latency, value, now),
            type='time')
    payload['time'] = value
    return payload


@validator
def validate_registration(payload, received_at):
    value = str(payload['registration'])
    if len(value) > 50:
        raise ContentException(
            'Registration value is too long: "{}"'.format(value),
            type='registration')
    payload['registration'] = value
    return payload


@validator
def validate_coordinates(payload, received_at):
    value = payload['coordinates']
    if len(value) != 3:
        raise FormatException(
            'Coordinates must have lat/lon/alt: "{}"'.format(value),
            type='coordinates')
    lat, lon, alt = value
    try:
        lat, lon, alt = float(lat), float(lon), float(alt)
    except ValueError:
        raise FormatException(
            'Coordinates must be floats: "{}"'.format(value),
            type='coordinates')
    errors = []
    if not -90 < lat < 90:
        errors.append('Latitude must be within [-90, 90]: {}'.format(value))
    if not -180 < lon < 180:
        errors.append('Longitude must be within [-180, 180]: {}'.format(value))
    if not alt < 9000:
        # Because https://www.youtube.com/watch?v=SiMHTK15Pik
        errors.append("It's over 9000!!! {}".format(value))
    if errors:
        raise FormatException(' '.join(errors), type='coordinates')
    payload['coordinates'] = [lat, lon, alt]
    return payload


@validator
def check_coordinates(payload, received_at):
    url = CHECK_URL.format(*payload['coordinates'])
    try:
        response = requests.get(url, timeout=3)
        payload['allowed'] = response.json()['allowed']
        payload['allowed_at'] = datetime.now(timezone.utc)
        return payload
    except (requests.exceptions.RequestException,
            json.decoder.JSONDecodeError):
        payload['allowed'] = None
        payload['allowed_at'] = None
        return payload
