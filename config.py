import os

import psycopg2
import redis

DB_CONFIG = {
    'database': os.environ.get('POSTGRES_DB', 'squirrel'),
    'host': os.environ.get('POSTGRES_HOST', None),
    'user': os.environ.get('POSTGRES_USER', None),
    'password': os.environ.get('POSTGRES_PASSWORD', None),
}
POSTGRES = psycopg2.connect(**DB_CONFIG)

REDIS_DB = 0
REDIS_PORT = 6379
REDIS_HOST = os.environ.get('REDIS_HOST', 'localhost')
REDIS = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

CHECK_URL = (
    'https://drone.api.gouv.fr/flightareas/check?lon={0}&lat={1}&alt={2}')
