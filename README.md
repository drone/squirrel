# Squirrel

Python service that converts previously stored payloads into geographic data stored in PostGIS.

See the [meta respository](https://framagit.org/drone/meta) to undestand
the big picture.

## Constraints

It has to be fast and asynchronuous.

## Install

### PostgreSQL with PostGIS extension

```
$ createdb squirrel
$ psql squirrel
# CREATE EXTENSION postgis;
$ python cli.py initdb
```


### [Wolwerine](https://framagit.org/drone/wolverine) with at least one message stored in Redis

## Run

```
$ python cli.py process
```
