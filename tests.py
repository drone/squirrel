import json
from datetime import datetime, timedelta, timezone

import requests
import responses

from config import CHECK_URL
from conftest import ALLOWED_POSITION
from main import process_queue
from validators import validate


def test_simple_payload(factory, count, fetchone, check_allowed):
    now = datetime.now(timezone.utc)
    factory(time=now.isoformat(), registration='1234',
            coordinates=ALLOWED_POSITION)
    process_queue()
    assert count() == 1
    result = fetchone()
    assert result['time'] == now
    assert result['registration'] == '1234'
    assert result['coordinates'].coords == (43.57, 5.71, 0)
    assert count('reports') == 0


def test_can_pass_timestamp_as_time(factory, count, fetchone, check_allowed):
    now = datetime.now(timezone.utc)
    factory(time=now.timestamp())
    process_queue()
    assert count() == 1
    result = fetchone()
    assert result['time'] == now
    assert count('reports') == 0


def test_unparsable_time_doesnt_crash(factory, count, fetchone, check_allowed):
    factory(time='not a date')
    process_queue()
    assert count() == 0
    assert count('reports') == 1
    error = fetchone('reports')
    assert json.loads(bytes(error['data']).decode())['time'] == 'not a date'
    assert 'Invalid time "not a date"' in error['errors']['time']


def test_missing_time_does_not_crash(redis, count, fetchone):
    redis.zadd('queue', 123456789,
               '{"registration": "1234", "coordinates": [1, 2, 3]}')
    process_queue()
    assert count() == 0
    assert count('reports') == 1
    error = fetchone('reports')
    assert b'"registration": "1234"' in bytes(error['data'])
    assert 'Missing mandatory keys' in error['errors']['format']


def test_invalid_json_does_not_crash(redis, count, fetchone):
    redis.zadd('queue', 123456789, '{')
    process_queue()
    assert count() == 0
    assert count('reports') == 1
    error = fetchone('reports')
    assert bytes(error['data']) == b'{'
    assert error['errors']['format'] == "Malformed payload b'{'"


def test_invalid_twice_create_unique_error(redis, count):
    redis.zadd('queue', 123456789, '{')
    redis.zadd('queue', 123456789, '{')
    process_queue()
    assert count('reports') == 1


def test_validate_invalid_payload():
    _, errors = validate(b'{', 1)
    assert 'Malformed payload' in errors['format']


def test_validate_missing_keys():
    _, errors = validate(b'{"coordinates": ""}', 1)
    assert 'Missing mandatory keys' in errors['format']


def test_validate_too_many_extra_keys():
    _, errors = validate(b'{"coordinates": "", "registration": "", '
                         b'"time": "", "foo": ""}', 1)
    assert 'Too many keys' in errors['format']


def test_validate_invalid_time(factory, check_allowed):
    payload = factory(**{'time': 'foo'})
    _, errors = validate(payload, 1)
    assert 'Invalid time "foo"' in errors['time']


def test_validate_future_time(factory, check_allowed):
    payload = factory(**{
        'time': (datetime.now(timezone.utc) +
                 timedelta(seconds=301)).isoformat()
    })
    _, errors = validate(payload, 1)
    assert 'Time is in the future (might be a timezone issue' in errors['time']


def test_validate_past_time(factory, check_allowed):
    now = datetime.now(timezone.utc)
    payload = factory(**{
        'time': (now - timedelta(minutes=31)).isoformat(),
    })
    received_at = now.timestamp()
    _, errors = validate(payload, received_at)
    assert 'Time is in the past' in errors['time']


def test_validate_registration_too_long(factory, check_allowed):
    payload = factory(**{'registration': '*' * 51})
    _, errors = validate(payload, 1)
    assert 'Registration value is too long' in errors['registration']


def test_validate_coordinates_invalid(factory):
    payload = factory(**{'coordinates': [1, 2]})
    _, errors = validate(payload, 1)
    assert 'Coordinates must have lat/lon/alt' in errors['coordinates']


def test_validate_coordinates_invalid_not_float(factory):
    payload = factory(**{'coordinates': ['1A', 2, 3]})
    _, errors = validate(payload, 1)
    assert 'Coordinates must be floats' in errors['coordinates']


def test_validate_coordinates_as_strings(factory, check_allowed):
    payload = factory(**{'coordinates': ['43.57', '5.71', '0.0']})
    valid_payload, errors = validate(payload, 1)
    assert 'coordinates' not in errors
    assert valid_payload['coordinates'] == ALLOWED_POSITION


def test_validate_coordinates_out_of_range_latitude(factory):
    payload = factory(**{'coordinates': [91, 0, 0]})
    _, errors = validate(payload, 1)
    assert 'Latitude must be within [-90, 90]' in errors['coordinates']


def test_validate_coordinates_out_of_range_longitude(factory):
    payload = factory(**{'coordinates': [0, 181, 0]})
    _, errors = validate(payload, 1)
    assert ('Longitude must be within [-180, 180]'
            in errors['coordinates'])


def test_validate_coordinates_out_of_range_altitude(factory):
    payload = factory(**{'coordinates': [0, 0, 9001]})
    _, errors = validate(payload, 1)
    assert "It's over 9000!!!" in errors['coordinates']


def test_valid_payload_should_not_return_errors(factory, check_allowed):
    payload = factory()
    received_at = datetime.now(timezone.utc).timestamp()
    _, errors = validate(payload, received_at)
    assert errors == {}


def test_validate_registration_as_int(factory, check_allowed):
    payload = factory(**{'registration': 123456789})
    _, errors = validate(payload, 1)
    assert errors == {}


def test_check_coordinates_populates_allowed(factory, check_allowed):
    payload = factory()
    valid_payload, errors = validate(payload, 1)
    assert errors == {}
    assert valid_payload['allowed'] is True
    assert valid_payload['allowed_at'] is not None


@responses.activate
def test_check_coordinates_return_false_on_forbidden_area(factory):
    coordinates = [43.51, 5.35, 0.0]  # CTR AIX.
    payload = factory(**{'coordinates': coordinates})
    url = CHECK_URL.format(*coordinates)
    responses.add(responses.GET, url, json={'allowed': False},
                  match_querystring=True)
    valid_payload, errors = validate(payload, 1)
    assert errors == {}
    assert valid_payload['allowed'] is False
    assert valid_payload['allowed_at'] is not None


@responses.activate
def test_check_coordinates_resilient_on_server_failure(factory):
    payload = factory(**{'coordinates': ALLOWED_POSITION})
    url = CHECK_URL.format(*ALLOWED_POSITION)
    responses.add(responses.GET, url, match_querystring=True,
                  body=requests.exceptions.RequestException())
    valid_payload, errors = validate(payload, 1)
    assert errors == {}
    assert valid_payload['allowed'] is None
    assert valid_payload['allowed_at'] is None


def test_creating_report_for_existing_key_updates(redis, count, fetchone):
    redis.zadd('queue', 123456789, '{')  # Invalid payload.
    process_queue()
    assert count() == 0
    assert count('reports') == 1
    error = fetchone('reports')
    assert error['time'].timestamp() == 123456789
    redis.zadd('queue', 123456790, '{')
    process_queue()
    assert count('reports') == 1
    error = fetchone('reports')
    assert error['time'].timestamp() == 123456790
    assert error['errors']['format'] == "Malformed payload b'{'"
    assert bytes(error['data']) == b'{'


def test_does_not_fail_on_nul_terminated_strings(redis, count, check_allowed):
    redis.zadd('queue', 123456789,
               '{{\n"registration": "1234",\n"time": {},'
               '\n"coordinates": {}'
               '\n}}\n\x00'.format(
                   datetime.now(timezone.utc).timestamp(), ALLOWED_POSITION))
    process_queue()
    assert count() == 1


def test_report_with_nul_terminated_string_does_not_fail(redis, count):
    redis.zadd('queue', 123456789,
               '{"invalid json",\n}\n\x00')
    process_queue()
    assert count() == 0
    assert count('reports') == 1
